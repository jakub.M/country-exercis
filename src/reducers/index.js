import { combineReducers } from 'redux';
import { reducer as reducerForm } from 'redux-form';
import countryReducer from './countryReducer';

const rootReducer = combineReducers({
  countryReducer,
  form: reducerForm,
});

export default rootReducer;
