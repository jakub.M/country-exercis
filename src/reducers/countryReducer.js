import { SET_CITIES, SET_CITY_DESCRIPTION, CLOSE_EDITING } from '../actions/types';

const initialState = {
  cities: [],
  cityDescription: '',
  isEditing: false,
  header: 'The most poluted cities list',
  country: '',
  title: 'No country chosen!',
};

const countryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_CITIES:
      return { ...state, cities: payload.cities, country: payload.country, title: payload.country, isEditing: false };
    case SET_CITY_DESCRIPTION:
      return { ...state, cityDescription: payload.extract, isEditing: true, title: payload.title };
    case CLOSE_EDITING:
      return { ...state, isEditing: false, title: state.country };
    default:
      return { ...state };
  }
};

export default countryReducer;
