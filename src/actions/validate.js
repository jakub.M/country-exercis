import { includes, toLower } from 'lodash';

const posibleCountries = ['poland', 'spain', 'germany', 'france'];

export const validate = values => {
  const errors = {};
  if (!includes(posibleCountries, toLower(values.country))) {
    errors.country = 'Not work with tihs country';
  }
  if (!values.country) {
    errors.country = 'Reqired';
  }
  return errors;
};
