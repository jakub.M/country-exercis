import axios from 'axios';
import { getCode } from 'country-list';
import { filter, map, sortBy, uniq, take, startCase } from 'lodash';
import { SET_CITIES, SET_CITY_DESCRIPTION, CLOSE_EDITING } from './types';

const apiCountry = axios.create({ baseURL: 'https://api.openaq.org/v1/' });
const apiCitesDescription = axios.create({
  baseURL: 'https://pl.wikipedia.org/w',
});

const params = {
  origin: '*',
  action: 'query',
  prop: 'extracts',
  exintro: 1,
  explaintext: 1,
  continue: '',
  format: 'json',
  formatversion: 2,
};

export const setCities = cities => ({
  type: SET_CITIES,
  payload: cities,
});

export const closeEditing = () => ({
  type: CLOSE_EDITING,
});

export const setCityDesctiption = cityDesctiption => ({
  type: SET_CITY_DESCRIPTION,
  payload: cityDesctiption,
});

const selectedCitise = cities => filter(cities, city => city.parameter === 'pm10');
const reducedCiteies = cities => map(selectedCitise(cities), item => ({ city: item.city, value: item.value }));
const soretedCities = cities => sortBy(reducedCiteies(cities), ['value']);
const onlyNameCitiesReduced = cities => map(soretedCities(cities), item => item.city);
const tenOnlyNameCities = cities => take(onlyNameCitiesReduced(cities), 10);
const citiesResult = cities => uniq(tenOnlyNameCities(cities));

export const fetchCities = event => {
  return dispatch => apiCountry.get('measurements', {
    params: {
      country: getCode(event.country),
    }
  })
    .then(res => dispatch(setCities({ cities: citiesResult(res.data.results), country: startCase(event.country) })))
    .catch(err => console.log(err));
};

export const fetchCityDescription = cityName => {
  return dispatch => apiCitesDescription.get('/api.php', { params: { ...params, titles: cityName } })
    .then(res => dispatch(setCityDesctiption(res.data.query.pages[0])))
    .catch(err => console.log(err));
};
