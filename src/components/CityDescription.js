import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import { FaAngleDoubleLeft } from 'react-icons/fa';

const CityDescription = ({ description, closeEditing }) => (
  <div style={{ padding: '3rem' }}>
    <Button onClick={closeEditing}><FaAngleDoubleLeft size={20} /></Button>
    <p style={{ marginTop: '2rem' }}>{description}</p>
  </div>
);

CityDescription.propTypes = {
  description: PropTypes.string.isRequired,
  closeEditing: PropTypes.func.isRequired,
};

export default CityDescription;
