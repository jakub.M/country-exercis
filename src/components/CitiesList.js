import React from 'react';
import PropTypes from 'prop-types';
import { ListGroup } from 'react-bootstrap';

const CitiesList = ({ cities, fetchCityDescription }) => (
  <div>
    {cities.length ? (
      <ListGroup style={{ marginTop: '8vh', marginRight: '4vw', marginLeft: '4vw', boxShadow: '0px 2px 5px 1px rgba(0, 0, 0, 0.1)' }}>
        {cities.map((name, i) => (
          <ListGroup.Item
            key={i}
            action
            onClick={() => fetchCityDescription(name)}
            variant="ligth"
            style={{ height: '6vh' }}
          >
            {name}
          </ListGroup.Item>
        ))}
      </ListGroup>
    ) : ''}
  </div>
);

CitiesList.propTypes = {
  cities: PropTypes.array.isRequired,
  fetchCityDescription: PropTypes.func.isRequired,
};

export default CitiesList;
