import React from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { Form, Button } from 'react-bootstrap';
import { Input } from './container/Input';


const Search = ({ handleSubmit, onSubmit }) => (
  <Form inline onSubmit={handleSubmit(onSubmit)}>
    <Field
      name="country"
      type="text"
      component={Input}
      placeholder="Country"
    />
    <Button variant="outline-light" type="submit">Search</Button>
  </Form>
);

Search.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default Search;
