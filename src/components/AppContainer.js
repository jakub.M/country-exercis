import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Navbar, Card } from 'react-bootstrap';

import CityDescriptionContainer from './CityDescriptionContainer';
import CitiesListContainer from './CitiesListContainer';
import SearchContainer from './SearchContainer';

const AppContainer = ({ isEditing, header, title }) => (
  <div>
    <Navbar bg="primary" variant="ligth" style={{ boxShadow: '0px 3px 4px 1px rgba(0, 0, 0, 0.3)' }}>
      <SearchContainer />
    </Navbar>
    <Card style={{ marginTop: '5vw', marginLeft: '15vw', marginRight: '15vw', minHeight: '85vh', textAlign: 'center' }}>
      <Card.Header style={{ boxShadow: '2px 0px 5px 1px rgba(0, 0, 0, 0.2)' }}>{header}</Card.Header>
      <Card.Body style={{ boxShadow: ' 2px 2px 5px 1px rgba(0, 0, 0, 0.2)' }}>
        <Card.Title>{title}</Card.Title>
        {isEditing ? <CityDescriptionContainer /> : <CitiesListContainer />}
      </Card.Body>
    </Card>
  </div>
);

AppContainer.propTypes = {
  isEditing: PropTypes.bool.isRequired,
  header: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isEditing: state.countryReducer.isEditing,
  header: state.countryReducer.header,
  title: state.countryReducer.title
});


export default connect(mapStateToProps, null)(AppContainer);
