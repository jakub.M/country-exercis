import React from 'react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Search from './Search';
import { fetchCities } from '../actions/country';
import { validate } from '../actions/validate';

const SearchContainer = ({ fetchCities, handleSubmit }) => (
  <Search
    handleSubmit={handleSubmit}
    onSubmit={fetchCities}
  />
);

SearchContainer.propTypes = {
  fetchCities: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchCities,
}, dispatch);

const mapStateToProps = () => ({

});

const formSearchContainer = reduxForm({ form: 'Country', validate })(SearchContainer);

export default connect(mapStateToProps, mapDispatchToProps)(formSearchContainer);
