import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';


export const Input = ({ input, meta: { touched, error } }) => (
  <Form.Control {...input} className="mr-sm-3" isInvalid={touched && error} style={{ marginLeft: '10rem' }} />
);

Input.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};
