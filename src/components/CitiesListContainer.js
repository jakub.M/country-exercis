import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CitiesList from './CitiesList';
import { fetchCityDescription } from '../actions/country';

const CitiesListContainer = props => (
  <CitiesList {...props} />
);

const mapStateToProps = state => ({
  cities: state.countryReducer.cities,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchCityDescription,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CitiesListContainer);
