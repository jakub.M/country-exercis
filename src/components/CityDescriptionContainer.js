import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import CityDescription from './CityDescription';
import { closeEditing } from '../actions/country';

const CityDescriptionContainer = props => (
  <CityDescription {...props} />
);

const mapStateToProps = state => ({
  description: state.countryReducer.cityDescription,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  closeEditing,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CityDescriptionContainer);
