import countryReducer from '../reducers/countryReducer'
import * as types from '../actions/types'

describe('event reducer', () => {
  it('should return initial state',()=>{
    expect(countryReducer(undefined,{})).toEqual(
      {
        cities: [],
        cityDescription: '',
        isEditing: false,
        header: 'The most poluted cities list',
        country: '',
        title: 'No country chosen!',
      })
    })

    it('',() =>{
      expect(
        countryReducer({},{
          type: types.CLOSE_EDITING
        })
      ).toEqual({
        isEditing: false,
      })
    })

    it('should handle SET_CITIES',() =>{
      expect(
        countryReducer({},{
          type: types.SET_CITIES,
          payload: { cities: [{},{}], country: 'country' },
        })
      ).toEqual({
        cities: [{},{}],
        country: 'country',
        title: 'country',
        isEditing: false,
      })
  })

    it('should handle SET_CITY_DESCRIPTION',() =>{
      expect(
        countryReducer({},{
          type: types.SET_CITY_DESCRIPTION,
          payload: { extract: 'description', title: 'country' },
        })
        ).toEqual({
          cityDescription: 'description',
          title: 'country',
          isEditing: true,
        })
    })
})