import * as actions from '../actions/country';
import * as types from '../actions/types';

describe('actions', () => {
    it('should create action to closing descriptions', () => {
        const exeptedAction = {
            type: types.CLOSE_EDITING,
        }
        expect(actions.closeEditing()).toEqual(exeptedAction)
    })

    it('should create action to set cities array of objects', () => {
        const payload = [{},{}];
        const exeptedAction = {
            type: types.SET_CITIES,
            payload,
        }
        expect(actions.setCities([{},{}])).toEqual(exeptedAction)
    })

    it('should create action set city description', () => {
        const payload = 'city description'
        const exeptedAction = {
            type: types.SET_CITY_DESCRIPTION,
            payload
        }
        expect(actions.setCityDesctiption('city description')).toEqual(exeptedAction)
    })
})