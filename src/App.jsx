import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AppContainer from './components/AppContainer';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={AppContainer} />
    </Switch>
  </BrowserRouter>
);

export default App;
