# 1. Description

An application to find the most polluted cities in different countries and information about these cities. 

Based on https://docs.openaq.org/#api-Measurements and https://www.mediawiki.org/wiki/API:Query

# 2. Setup

````
git clone https://gitlab.com/jakub.M/country-exercis.git
cd country-exercis
npm install
````

# 3. Run
````
npm start
````
open browser on http://localhost:3100

# 5. Test
````
npm test
````
# 6. build
````
npm build
````
